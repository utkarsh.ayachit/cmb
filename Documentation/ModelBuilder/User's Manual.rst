User's Manual
=============

.. toctree::
   :maxdepth: 2

   Getting ModelBuilder
   What is ModelBuilder
   Getting Started
   Loading and Saving Simulations
   Session Operators

.. todo::
	Add more use case documentation
