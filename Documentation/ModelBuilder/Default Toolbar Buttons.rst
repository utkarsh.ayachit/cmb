
.. index:: Toolbars

Default Toolbar Buttons
=======================

ModelBuilder by default starts with four toolbars open: File IO, Camera Controls, Selection, and Color.

File IO
^^^^^^^
|pqOpen32| Open a file.

.. Note::
	to open a file, a :ref:`plugin <load-plugins>` with the appropriate reader must be loaded

Camera Controls
^^^^^^^^^^^^^^^

|pqResetCamera32| Reset the camera to the default view

|pqXMinus24| |pqXPlus24| |pqYMinus24| |pqYPlus24| |pqZMinus24| |pqZPlus24| Change the camera to the respective axis

|pqZoomToBox24| Zoom the camera view to a user-selected box

|pqZoomToSelection24| Zoom the camera view to a user-selected box and select the elements within the box

|pqShowCenterAxes24| Show a 3D orientation indicator of the camera's center

|pqResetCenter24| Reset the camera center to the default

|pqPickCenter24| Allow the user to manually click and define a camera center

|LinkCenterAndFocal| Make future manipulations of the camera view rotate around the current camera center.

|CameraManipulationStyle| Toggle between 2D and 3D camera manipulation

Selection
^^^^^^^^^

|pqSelect32| Allow the user to select an objects and faces with a box

Color
^^^^^

|ColorBy| Color each {entity, group, volume, attribute} with a unique color

|pqScalarBar24| Show a legend of every item and its corresponding color

.. |CameraManipulationStyle| image:: images/CameraManipulationStyle.png
.. |ColorBy| image:: images/ColorBy.png
.. |LinkCenterAndFocal| image:: images/LinkCenterAndFocal.png
.. |ModelBuilderIcon| image:: images/ModelBuilderIcon.png
.. |pqOpen32| image:: images/pqOpen32.png
	:scale: 75%
.. |pqPickCenter24| image:: images/pqPickCenter24.png
.. |pqResetCamera32| image:: images/pqResetCamera32.png
	:scale: 75%
.. |pqResetCenter24| image:: images/pqResetCenter24.png
.. |pqScalarBar24| image:: images/pqScalarBar24.png
.. |pqSelect32| image:: images/pqSelect32.png
	:scale: 75%
.. |pqShowCenterAxes24| image:: images/pqShowCenterAxes24.png
.. |pqXMinus24| image:: images/pqXMinus24.png
.. |pqXPlus24| image:: images/pqXPlus24.png
.. |pqYMinus24| image:: images/pqYMinus24.png
.. |pqYPlus24| image:: images/pqYPlus24.png
.. |pqZMinus24| image:: images/pqZMinus24.png
.. |pqZoomToBox24| image:: images/pqZoomToBox24.png
.. |pqZoomToSelection24| image:: images/pqZoomToSelection24.png
.. |pqZPlus24| image:: images/pqZPlus24.png

